const { Router } = require('express')
const { imageUpload } = require('../controllers/upload.controller')
const router = Router()

// POST /api/upload/image
router.post('/image', imageUpload)

module.exports = router
