const { Router } = require('express')
const ctr = require('../controllers/download.controller')
const router = Router()

router.get('/', ctr.downloadPrice)

module.exports = router
