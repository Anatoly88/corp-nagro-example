const { Router } = require('express')

const {
  createCandidate,
  checkCandidate
} = require('../controllers/candidate.controller')

const router = Router()

// POST /api/candidate/create
router.post('/create', createCandidate)

// POST /api/candidate/check
router.post('/check', checkCandidate)

module.exports = router
