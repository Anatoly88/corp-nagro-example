const { Router } = require('express')
// const passport = require('passport')
const {
  createNew,
  getAll,
  getNew,
  updateNew,
  removeNew
} = require('../controllers/new.controller')
const router = Router()

//  GET /api/news/list
router.get('/list', getAll)

//  GET /api/news/:id
router.get('/:id', getNew)

//  POST /api/news/create
router.post('/create', createNew)

//  PUT /api/news/update
router.put('/update/:slug', updateNew)

//  DEL /api/news/remove/:id
router.delete('/remove/:id', removeNew)

module.exports = router
