const { Router } = require('express')
const {
  subscribeUser,
  subscribersList
} = require('../controllers/subscribe.controller')
const router = Router()

//  POST /api/subscribe/new
router.post('/new', subscribeUser)

//  GET /api/subscribe/list
router.get('/list', subscribersList)

module.exports = router
