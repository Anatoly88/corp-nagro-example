const { Router } = require('express')
const {
  createAction,
  updateAction,
  getAll,
  getById,
  remove
} = require('../controllers/action.controller')
const router = Router()

//  POST /api/action/create
router.post('/create', createAction)

//  PUT /api/action/update/:id
router.put('/update/:id', updateAction)

//  GET /api/action/all
router.get('/all', getAll)

//  GET /api/action/:id
router.get('/:id', getById)

//  DELETE /api/action/remove/:id
router.delete('/remove/:id', remove)

module.exports = router
