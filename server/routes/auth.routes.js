const { Router } = require('express')

const {
  login,
  createUser,
  updateUser,
  loginAdmin,
  createAdmin
} = require('../controllers/auth.contoroller')
const router = Router()

// path /api/auth/admin/create
router.post('/admin/create', createAdmin)

// path /api/auth/admin/login
router.post('/admin/login', loginAdmin)

// path /api/auth/login
router.post('/login', login)

// path /api/auth/create
router.post('/create', createUser)

// path /api/auth/update
router.put('/update', updateUser)

module.exports = router
