const { Router } = require('express')
const {
  mailPasswordRec,
  mailSignUp
} = require('../controllers/mail.controller')
const router = Router()

// /api/mail/recovery
router.post('/recovery', mailPasswordRec)
// /api/mail/sign-up
router.post('/sign-up', mailSignUp)

module.exports = router
