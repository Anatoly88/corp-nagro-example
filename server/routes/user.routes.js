const { Router } = require('express')
const {
  getUserData,
  avatarFileName
} = require('../controllers/user.controller')
const upload = require('../middleware/upload')
const router = Router()

// GET /api/users/:id
router.get('/:id', getUserData)

// POST /api/user/upload
router.post('/upload', upload.single('image'), avatarFileName)

module.exports = router
