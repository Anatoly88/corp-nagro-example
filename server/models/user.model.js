const { model, Schema } = require('mongoose')

const userSchema = new Schema({
  login: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true,
    minLength: 6
  },
  role: {
    type: String
  },
  imageUrl: {
    type: String
  },
  name: {
    type: String
  },
  lastName: {
    type: String
  },
  company: {
    type: String
  },
  dateReg: {
    type: String
  },
  phone: {
    type: String
  },
  email: {
    type: String
  },
  skype: {
    type: String
  },
  whatsapp: {
    type: String
  },
  telegram: {
    type: String
  },
  country: {
    type: String
  },
  gender: {
    type: Object
  },
  birthday: {
    type: String
  }
})

module.exports = model('User', userSchema)
