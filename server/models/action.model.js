const { model, Schema } = require('mongoose')

const actionModel = new Schema({
  imageUrl: {
    type: String,
    required: false
  },
  content: {
    type: String,
    required: true,
    minLength: 3
  }
})

module.exports = model('Action', actionModel)
