const { Schema, model } = require('mongoose')

const Subsriber = new Schema({
  email: {
    type: String,
    required: true
  }
})

module.exports = model('subscribers', Subsriber)
