const { model, Schema } = require('mongoose')

const candidateModel = new Schema({
  code: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now,
    index: {
      unique: true,
      expires: '30s'
    }
  }
})

module.exports = model('Candidate', candidateModel)
