const { Schema, model } = require('mongoose')

const pageSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  value: {
    type: String,
    required: true
  },
  equipmentType: {
    type: String,
    required: true
  },
  imageSrc: {
    type: String,
    required: false
  },
  price: {
    type: String,
    required: true
  },
  performance: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  parametrs: {
    type: String,
    required: true
  },
  footer: {
    type: String,
    required: true
  }
})

module.exports = model('equipments', pageSchema)
