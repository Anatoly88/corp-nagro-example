const moment = require('moment')
const Article = require('../models/article.model')

module.exports.createArticle = async (req, res) => {
  const article = new Article({
    title: req.body.title,
    slug: req.body.slug,
    description: req.body.description,
    content: req.body.content,
    coverUrl: req.body.coverUrl,
    date: moment().format(req.body.date)
  })

  try {
    await article.save()
    res.status(201).json(req.body)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.updateArticle = async (req, res) => {
  const $set = {
    title: req.body.title,
    slug: req.body.slug,
    description: req.body.description,
    content: req.body.content,
    coverUrl: req.body.coverUrl,
    date: moment().format(req.body.date)
  }

  try {
    const article = await Article.findOneAndUpdate(
      {
        slug: req.params.slug
      },
      { $set },
      { new: true }
    )
    res.json(article)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.getAll = async (req, res) => {
  try {
    const articles = await Article.find()
    res.json(articles.reverse())
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.getArticle = async (req, res) => {
  try {
    const article = await Article.findOne({ slug: req.params.id })
    res.json(article)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.removeArticle = async (req, res) => {
  try {
    await Article.deleteOne({ _id: req.params.id })
    res.json({ message: 'Статья удалена' })
  } catch (e) {
    res.status(500).json(e)
  }
}
