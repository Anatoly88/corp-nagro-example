const Candidate = require('../models/candidate.model')
const User = require('../models/user.model')
const smsc = require('../smsc_api/smsc_api')

module.exports.createCandidate = async (req, res) => {
  const candidate = await User.findOne({ login: req.body.phone })

  if (candidate) {
    res.status(409).json({ message: 'Такой логин уже занят' })
  } else {
    try {
      const getRandomIntInclusive = (min, max) => {
        min = Math.ceil(min)
        max = Math.floor(max)
        return Math.floor(Math.random() * (max - min + 1)) + min
      }

      const number = getRandomIntInclusive(1000, 9999)

      const candidate = new Candidate({
        code: number,
        phone: req.body.phone
      })

      await candidate.save()

      const candidateId = candidate._id
      const candidateCode = candidate.code
      const candidatePhone = candidate.phone

      await smsc.configure({
        login: '',
        password: '',
        ssl: true,
        charset: 'utf-8'
      })

      // Отправка SMS
      await smsc.send_sms(
        {
          phones: [candidatePhone],
          mes: `Ваш код подтверждения на сайте nagro.group ${candidateCode}`
        },
        function(data, raw, err, code) {
          if (err) return console.log(err, 'code: ' + code)
          res.status(201).json({ candidateId })
        }
      )

      // await client.messages
      //   .create({
      //     from: fromPhone,
      //     to: candidatePhone,
      //     body: `${candidateCode} - Код подтверждения nagro.group`
      //   })
      //   .then(() => {
      //     res.status(201).json({ candidateId })
      //   })
      //   .catch((e) => {
      //     res.status(500).json(e.message)
      //   })
    } catch (e) {
      res.status(500).json(e.message)
    }
  }
}

module.exports.checkCandidate = async (req, res) => {
  try {
    const candidate = await Candidate.findOne({ _id: req.body.id || null })
    const code = await req.body.code

    if (candidate) {
      if (candidate.code === code) {
        res.status(200).json({ isConfirmed: true })
      } else if (!code) {
        throw new Error('Нет поля code в теле запроса')
      } else {
        throw new Error('Неверный код')
      }
    } else {
      throw new Error('Кандидат устарел')
    }
  } catch (err) {
    res.status(500).json({ error: err.message })
  }
}
