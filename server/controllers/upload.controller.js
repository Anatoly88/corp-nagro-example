const path = require('path')
const moment = require('moment')
const pathUpload = path.resolve(__dirname, '../../', 'static/upload')

module.exports.imageUpload = (req, res) => {
  try {
    if (!req.files) {
      return res.status(500).send({ msg: 'Файл не найден' })
    }

    const myFile = req.files.file
    const fileName = `${moment().format('DD_MM_YYYY_HH_mm')}_${myFile.name}`

    myFile.mv(`${pathUpload}/${fileName}`, function(err) {
      if (err) {
        return res.status(500).send({ msg: 'Ошибка - ' + err + pathUpload })
      }

      return res.status(200).send({
        url: `/upload/${fileName}`
      })
    })
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.imageFileName = (req, res) => {
  console.log(req)
  try {
    const imageUrl = { imageUrl: req.file.filename }
    res.status(200).json(imageUrl)
  } catch (e) {
    res.status(500).json(e)
  }
}
