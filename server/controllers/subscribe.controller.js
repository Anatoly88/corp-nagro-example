const Subsriber = require('../models/subscribe.model')

module.exports.subscribeUser = async (req, res) => {
  const subscriber = new Subsriber({
    email: req.body.email
  })
  try {
    await subscriber.save()
    res.status(201).json(req.body)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.subscribersList = async (req, res) => {
  try {
    const subscribers = await Subsriber.find()
    res.json(subscribers.reverse())
  } catch (e) {
    res.status(500).json(e)
  }
}
