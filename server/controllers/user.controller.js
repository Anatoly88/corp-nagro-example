const User = require('../models/user.model')

module.exports.getUserData = async (req, res) => {
  try {
    const user = await User.findById(req.params.id)
    res.status(200).json(user)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.avatarFileName = (req, res) => {
  try {
    const avatarName = { imageUrl: req.file.filename }
    res.status(200).json(avatarName)
  } catch (e) {
    res.status(500).json(e)
  }
}
