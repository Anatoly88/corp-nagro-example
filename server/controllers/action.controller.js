const Action = require('../models/action.model')

module.exports.createAction = async (req, res) => {
  try {
    const action = new Action({
      imageUrl: req.body.imageUrl,
      content: req.body.content
    })

    await action.save()
    res.status(201).json(req.body)
  } catch (e) {
    res.status(500).json(e.message)
  }
}

module.exports.updateAction = async (req, res) => {
  const $set = {
    imageUrl: req.body.imageUrl,
    content: req.body.content
  }

  try {
    const action = await Action.findOneAndUpdate(
      {
        _id: req.params.id
      },
      { $set },
      { new: true }
    )
    res.json(action)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.getAll = async (req, res) => {
  try {
    const actions = await Action.find()

    res.json(actions.reverse())
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.getById = async (req, res) => {
  try {
    const action = await Action.findOne({ _id: req.params.id })

    res.json(action)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.remove = async (req, res) => {
  try {
    await Action.deleteOne({ _id: req.params.id })
    res.json({ message: 'Акция удалена' })
  } catch (e) {
    res.status(500).json(e)
  }
}
