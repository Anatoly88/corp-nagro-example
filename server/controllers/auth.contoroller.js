// const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const moment = require('moment')
const keys = require('../keys')
const User = require('../models/user.model')

module.exports.loginAdmin = async (req, res) => {
  const candidate = await User.findOne({ login: req.body.login })

  // проверяем если в модели юзера и теле запроса совпадает логин т.е логин есть в базе
  if (candidate) {
    const isPassword = req.body.password === candidate.password
    // проверяем сопадает ли пароль из тела запроса с паролем в базе
    if (candidate.role && candidate.role === 'admin') {
      if (isPassword) {
        const token = jwt.sign(
          {
            login: candidate.login,
            userId: candidate._id
          },
          keys.JWT,
          { expiresIn: 60 * 60 }
        )

        res.status(200).json({ token })
      } else {
        res.status(404).json({ message: 'Неверный пароль' })
      }
    } else {
      res
        .status(404)
        .json({ message: 'Пользователь не являеется админом сайта' })
    }
  } else {
    res.status(404).json({ message: 'Пользователь не найден' })
  }
}

module.exports.login = async (req, res) => {
  const candidate = await User.findOne({ login: req.body.login })
  // проверяем если в модели юзера и теле запроса совпадает логин т.е логин есть в базе
  if (candidate) {
    // const isPassword = bcrypt.compareSync(req.body.password, candidate.password)
    const isPassword = req.body.password
    // проверяем сопадает ли пароль из тела запроса с паролем в базе
    if (isPassword) {
      const token = jwt.sign(
        {
          login: candidate.login,
          userId: candidate._id
        },
        keys.JWT,
        { expiresIn: 60 * 60 }
      )
      const userId = candidate._id

      res.status(200).json({ token, userId })
    } else {
      res.status(404).json({ message: 'Неверный пароль' })
    }
  } else {
    res.status(404).json({ message: 'Пользователь не найден' })
  }
}

module.exports.createUser = async (req, res) => {
  const candidate = await User.findOne({ login: req.body.login })
  // проверяем если в модели юзера и теле запроса совпадает логин т.е логин есть в базе
  if (candidate) {
    res.status(409).json({ message: 'Такой логин уже занят' })
  } else {
    // вспомогательный хэш для шифрования пароля
    // TODO вернуть шифрование
    // const salt = bcrypt.genSaltSync(10)

    // создаем пользователя из модели User, пароль шифруем из тела запроса вторым параметром передаем хэш
    const user = new User({
      login: req.body.login,
      name: req.body.name,
      password: req.body.password || '',
      lastName: req.body.lastName || '',
      company: req.body.company || '',
      dateReg: moment().format('DD.MM.YYYY'),
      phone: req.body.phone || '',
      email: req.body.email || '',
      skype: req.body.skype || '',
      whatsapp: req.body.whatsapp || '',
      telegram: req.body.telegram || '',
      country: req.body.country || '',
      gender: req.body.gender || { male: false, female: false },
      birthday: req.body.birthday || '',
      imageUrl: req.body.image || ''
    })

    // сохраняем модель в базе

    await user.save()

    // отвечаем на клиент что успешно создали юзера и возвращаем объект юзера
    res.status(201).json(user)
  }
}

module.exports.updateUser = async (req, res) => {
  const $set = {
    login: req.body.login,
    name: req.body.name,
    password: req.body.password || '',
    lastName: req.body.lastName || '',
    company: req.body.company || '',
    dateReg: req.body.dateReg || '',
    phone: req.body.phone || '',
    email: req.body.email || '',
    skype: req.body.skype || '',
    whatsapp: req.body.whatsapp || '',
    telegram: req.body.telegram || '',
    country: req.body.country || '',
    gender: req.body.gender || { male: false, female: false },
    birthday: req.body.birthday || '',
    imageUrl: req.body.imageUrl || ''
  }
  try {
    const user = await User.findByIdAndUpdate(
      req.body._id,
      { $set },
      { new: true }
    )
    res.json(user)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.createAdmin = async (req, res) => {
  const candidate = await User.findOne({ login: req.body.login })
  // проверяем если в модели юзера и теле запроса совпадает логин т.е логин есть в базе
  if (candidate) {
    res.status(409).json({ message: 'Такой логин уже занят' })
  } else {
    // создаем пользователя из модели User, пароль шифруем из тела запроса вторым параметром передаем хэш

    const user = new User({
      login: req.body.login,
      name: req.body.name,
      password: req.body.password || '',
      lastName: req.body.lastName || '',
      company: req.body.company || '',
      dateReg: moment().format('DD.MM.YYYY'),
      phone: req.body.phone || '',
      email: req.body.email || '',
      skype: req.body.skype || '',
      whatsapp: req.body.whatsapp || '',
      telegram: req.body.telegram || '',
      country: req.body.country || '',
      gender: req.body.gender || { male: false, female: false },
      birthday: req.body.birthday || '',
      imageUrl: req.body.image || '',
      role: req.body.role || ''
    })

    // сохраняем модель в базе

    await user.save()

    // отвечаем на клиент что успешно создали юзера
    res.status(201).json(user)
  }
}

// module.exports.addCode = async (req, res) => {}
