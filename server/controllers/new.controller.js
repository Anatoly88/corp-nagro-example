const moment = require('moment')
const New = require('../models/new.model')

module.exports.createNew = async (req, res) => {
  const article = new New({
    title: req.body.title,
    slug: req.body.slug,
    description: req.body.description,
    content: req.body.content,
    coverUrl: req.body.coverUrl,
    date: moment().format(req.body.date)
  })

  try {
    await article.save()
    res.status(201).json(req.body)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.updateNew = async (req, res) => {
  const $set = {
    title: req.body.title,
    slug: req.body.slug,
    description: req.body.description,
    content: req.body.content,
    coverUrl: req.body.coverUrl,
    date: moment().format(req.body.date)
  }

  try {
    const news = await New.findOneAndUpdate(
      {
        slug: req.params.slug
      },
      { $set },
      { new: true }
    )
    res.json(news)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.getAll = async (req, res) => {
  try {
    const news = await New.find()
    res.json(news.reverse())
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.getNew = async (req, res) => {
  try {
    const news = await New.findOne({ slug: req.params.id })
    res.json(news)
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.removeNew = async (req, res) => {
  try {
    await New.deleteOne({ _id: req.params.id })
    res.json({ message: 'Статья удалена' })
  } catch (e) {
    res.status(500).json(e)
  }
}
