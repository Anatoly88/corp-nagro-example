const path = require('path')
module.exports.downloadPrice = (req, res) => {
  try {
    const downloadFileName = 'price_2021_06_18.pdf'
    res.setHeader(
      'Content-disposition',
      `attachment; filename=${downloadFileName}`
    )
    res.setHeader('Content-type', 'application/pdf')
    res.download(
      path.join(`./static/documents/${downloadFileName}`),
      downloadFileName,
      (err) => {
        if (err) console.log(err)
      }
    )
    // res.sendFile()
  } catch (e) {
    res.status(500).json(e)
  }
}
