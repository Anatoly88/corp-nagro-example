const Equipment = require('../models/equipments.model')

// создание страницы

module.exports.createEquipment = async (req, res) => {
  const equipment = new Equipment({
    title: req.body.title,
    value: req.body.value,
    equipmentType: req.body.equipmentType,
    imageSrc: `/${req.file.filename}`,
    price: req.body.price,
    performance: req.body.performance,
    content: req.body.content,
    parametrs: req.body.parametrs,
    footer: req.body.footer
  })

  try {
    await equipment.save()
    res.status(201).json(equipment)
  } catch (e) {
    res.status(500).json(e)
  }
}

// получаем данные всех страниц для вывода в админке
module.exports.getAllEquipments = async (req, res) => {
  try {
    const pages = await Equipment.find()
    res.json(pages)
  } catch (e) {
    res.status(500).json(e)
  }
}

// получаем данные конкретной страницы по id для вывода в поля редактирования
module.exports.getPage = async (req, res) => {
  try {
    // eslint-disable-next-line handle-callback-err
    await Equipment.findById(req.params.id).exec((error, page) => {
      res.json(page)
    })
  } catch (e) {
    res.status(500).json(e)
  }
}

// получаем данные конкретной страницы по имени для вывода в поля редактирования
module.exports.getEquipmentByName = async (req, res) => {
  try {
    // eslint-disable-next-line handle-callback-err
    await Equipment.findOne({ name: req.body.value }).exec((error, page) => {
      res.json(page)
    })
    // eslint-disable-next-line handle-callback-err
    // await Page.find({ name: req.params.name }).exec((error, page) => {
    //   console.log(page)
    //   res.json(page)
    // })
  } catch (e) {
    res.status(500).json(e)
  }
}

// обновляем данные оборудования
module.exports.updateEquipment = async (req, res) => {
  const $set = {
    title: req.body.title,
    value: req.body.value,
    price: req.body.price,
    performance: req.body.performance,
    content: req.body.content,
    parametrs: req.body.parametrs,
    footer: req.body.footer
  }

  try {
    const page = await Equipment.findOneAndUpdate(
      {
        _id: req.params.id
      },
      { $set },
      { new: true }
    )
    res.json(page)
  } catch (e) {
    res.status(500).json(e)
  }
}

// удаляем оборудование
module.exports.deleteEquipment = async (req, res) => {
  try {
    await Equipment.deleteOne({ _id: req.params.id })
    res.json({ message: 'Оборудование удалено' })
  } catch (e) {
    res.status(500).json(e)
  }
}
