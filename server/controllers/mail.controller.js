const nodemailer = require('nodemailer')
const User = require('../models/user.model')

module.exports.mailPasswordRec = async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email })

    if (user) {
      const login = user.login
      const password = user.password
      const email = req.body.email

      await recoveryPassword(login, password, email)

      res.status(200).json({ message: 'DONE' })
    } else {
      res.status(404).json({ message: 'Пользователь с таким email не найден' })
    }
  } catch (e) {
    res.status(500).json(e)
  }
}

module.exports.mailSignUp = async (req, res) => {
  try {
    const user = await User.findOne({ login: req.body.login })

    if (user) {
      const login = user.login
      const password = user.password
      const email = user.email

      await signUpData(login, password, email)

      res.status(200).json({ message: 'DONE' })
    } else {
      res.status(404).json({ message: 'Что-то пошло не так' })
    }
  } catch (e) {
    res.status(500).json(e)
  }
}

const recoveryPassword = (login, password, email) => {
  const transporter = nodemailer.createTransport({
    service: 'Yandex',
    auth: {
      user: '',
      pass: ''
    }
  })
  setTimeout(() => {
    const mailUser = email

    const htmlTemplateCompany = `
          <h1>Ваши логин и пароль</h1>
          <p style="padding: 10px; margin-bottom: 10px;">Логин: <b>${login ||
            ' '}</b></p>
          <p style="padding: 10px; margin-bottom: 10px;">Пароль: <b>${password ||
            ' '}</b></p>`

    transporter.sendMail({
      from: 'info@nagro.group',
      to: mailUser,
      subject: 'Восстановление пароля на сайте nagro.group',
      html: htmlTemplateCompany
    })
  }, 100)
}

const signUpData = (login, password, email) => {
  const transporter = nodemailer.createTransport({
    service: 'Yandex',
    auth: {
      user: '',
      pass: ''
    }
  })
  setTimeout(() => {
    const mailUser = email

    const htmlTemplateCompany = `
          <h1>Спасибо! Вы успешно зарегистрированны на сайте nagro.group</h1>
          <p style="font-size: 18px; padding: 10px; margin-bottom: 10px;">Данные для входа в личный кабинет:</p>
          <p style="padding: 10px; margin-bottom: 10px;">Логин: <b>${login ||
            ' '}</b></p>
          <p style="padding: 10px; margin-bottom: 10px;">Пароль: <b>${password ||
            ' '}</b></p>`

    transporter.sendMail({
      from: 'info@nagro.group',
      to: mailUser,
      subject: 'Регистрация на сайте nagro.group',
      html: htmlTemplateCompany
    })
  }, 4000)
}
