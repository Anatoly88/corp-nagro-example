import Vue from 'vue'
import VueIziToast from 'vue-izitoast'

const defaultOptionsObject = {
  error: {
    position: 'topRight',
    timeout: 3000
  },
  success: {
    position: 'topRight',
    timeout: 2000
  },
  position: 'topRight',
  timeout: 2000
}

Vue.use(VueIziToast, defaultOptionsObject)
