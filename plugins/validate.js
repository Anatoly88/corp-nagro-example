import Vue from 'vue'
import {
  ValidationProvider,
  extend,
  configure,
  ValidationObserver
} from 'vee-validate'
import * as rules from 'vee-validate/dist/rules'
import ru from 'vee-validate/dist/locale/ru.json'

// loop over all rules
for (const rule in rules) {
  extend(rule, {
    // eslint-disable-next-line import/namespace
    ...rules[rule], // add the rule
    message: ru.messages[rule] // add its message
  })
}

configure({
  classes: {
    valid: 'is-valid', // one class
    invalid: ['is-invalid'] // multiple classes
  }
})

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
// install the 'required' rule.
// extend('required', {
//   ...required,
//   message: 'This field is required'
// })
