const express = require('express')
const nodemailer = require('nodemailer')

const app = express()

app.use(express.json())

app.get('/', function(req, res) {
  res.status(405).json({
    error: 'sorry!'
  })
})

app.post('/', function(req, res, next) {
  // const attributes = ['name', 'email', 'phone']

  // const sanitizedAttributes = attributes.map((attribute) => {
  //   console.log(`attribute ${req.body[attribute]}`)
  //   validateAndSanitize(attribute, req.body[attribute])
  // })

  // const someInvalid = sanitizedAttributes.some((r) => !r)

  // res.json({
  //   message: sanitizedAttributes
  // })

  sendMail(
    req.body.name,
    req.body.phone,
    req.body.email,
    req.body.formName,
    req.body.company
  )

  res.status(200).json({
    message: `DONE ${req.body}`
  })

  // if (someInvalid) {
  //   return res.status(422).json({
  //     error: `error invalid ${res}`
  //   })
  // } else {
  //   sendMail(
  //     req.body.name,
  //     req.body.phone,
  //     req.body.email,
  //     req.body.formName,
  //     req.body.company
  //   )
  //   res.status(200).json({
  //     message: `DONE ${req.body}`
  //   })
  // }
})

module.exports = {
  path: '/api/contact',
  handler: app
}

// const validateAndSanitize = (key, value) => {
//   // обьект с полями в которых стрелочные функции которые возвращают булево значение
//   const rejectFunctions = {
//     name: (v) => v.length < 3,
//     email: (v) => !validator.isEmail(v),
//     phone: (v) => toString(v).length < 10
//   }

//   // If object has key and function returns false, return sanitized input. Else, return false
//   // если объект имеет ключ и функция возвращает false мы возвращаем значение input если нет возвращается false
//   return rejectFunctions.hasOwnProperty(key) && xssFilters.inHTMLData(value)
// }

const sendMail = (name, phone, email, formName, company) => {
  const transporter = nodemailer.createTransport({
    service: 'Yandex',
    auth: {
      user: 'callback@nagro.group',
      pass: '9SNyoVb336'
    }
  })
  setTimeout(() => {
    const mailList = [
      'anatolyhaulin@gmail.com',
      'info@nagro.group',
      'nagro.group@mail.ru'
    ]
    const htmlTemplateCompany = `
        <h1>Форма: ${formName || 'Не указано'}</h1>
        <p style="background-color:#ccc; padding: 10px; margin-bottom: 10px;">Имя: <b>${name ||
          'Не указано'}</b></p>
        <p style="background-color:#ccc; padding: 10px; margin-bottom: 10px;">Телефон: <b>${phone ||
          'Не указано'}</b></p>
        <p style="background-color:#ccc; padding: 10px; margin-bottom: 10px;">Email: <b>${email ||
          'Не указано'}</b></p>
        <p style="background-color:#ccc; padding: 10px; margin-bottom: 10px;">Название организации: <b>${company ||
          'Не указано'}</b></p>`

    transporter.sendMail({
      from: 'callback@nagro.group',
      // TODO не забыть вернуть почту
      // to: 'anatolyhaulin@gmail.com',
      to: mailList,
      subject: `Заявка с сайта nagro.group от ${name || 'Не указано'}`,
      html: htmlTemplateCompany
    })
  }, 100)
}
