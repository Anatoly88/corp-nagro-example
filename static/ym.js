/* eslint-disable */

;(function(m, e, t, r, i, k, a) {
  m[i] =
    m[i] ||
    function() {
      ;(m[i].a = m[i].a || []).push(arguments)
    }
  m[i].l = 1 * new Date()
  ;(k = e.createElement(t)),
    (a = e.getElementsByTagName(t)[0]),
    (k.async = 1),
    (k.src = r),
    a.parentNode.insertBefore(k, a)
})(window, document, 'script', 'https://mc.yandex.ru/metrika/tag.js', 'ym')
;(function(w, d, s, l, i) {
  w[l] = w[l] || []
  w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' })
  const f = d.getElementsByTagName(s)[0]
  const j = d.createElement(s)
  const dl = l != 'dataLayer' ? '&l=' + l : ''
  j.async = true
  j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl
  f.parentNode.insertBefore(j, f)
})(window, document, 'script', 'dataLayer', 'GTM-WPK943X')

!(function() {
  const t = document.createElement('script')
  ;(t.type = 'text/javascript'),
    (t.async = !0),
    (t.src = 'https://vk.com/js/api/openapi.js?168'),
    (t.onload = function() {
      VK.Retargeting.Init('VK-RTRG-551313-cA8y0'), VK.Retargeting.Hit()
    }),
    document.head.appendChild(t)
})()

!(function(f, b, e, v, n, t, s) {
  if (f.fbq) return
  n = f.fbq = function() {
    n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
  }
  if (!f._fbq) f._fbq = n
  n.push = n
  n.loaded = !0
  n.version = '2.0'
  n.queue = []
  t = b.createElement(e)
  t.async = !0
  t.src = v
  s = b.getElementsByTagName(e)[0]
  s.parentNode.insertBefore(t, s)
})(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js')
fbq('init', '706676300098615')
fbq('track', 'PageView')
