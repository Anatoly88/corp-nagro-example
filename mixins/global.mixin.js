export default {
  data: () => ({
    cover: '',
    image: null,
    validationRules: {
      title: [
        {
          required: true,
          message: 'Заголовок обязательное поле',
          trigger: 'blur'
        },
        { min: 3, message: 'Не меньше 3 букв', trigger: 'blur' }
      ],
      description: [
        {
          required: true,
          message: 'Описание обязательное поле',
          trigger: 'blur'
        },
        { min: 30, message: 'Не меньше 30 букв', trigger: 'blur' }
      ]
    }
  }),
  methods: {
    handleCoverChange(file) {
      const isJPG = file.raw.type === 'image/jpeg'
      const isPNG = file.raw.type === 'image/png'
      const isLt2M = file.size / 1024 / 1024 < 2

      if (!isJPG && !isPNG) {
        this.$message.error('Загрузите jpg или png файл не больше 2MB')
      }
      if (!isLt2M) {
        this.$message.error('Файл не должен превышать размер 2MB')
      }
      if ((isJPG && isLt2M) || (isPNG && isLt2M)) {
        this.cover = URL.createObjectURL(file.raw)
        this.image = file.raw
      }
    },
    eventsSendEmailADS() {
      // eslint-disable-next-line no-undef
      VK.Retargeting.Event('lead')
      // eslint-disable-next-line no-undef
      fbq('track', 'Lead')
      // eslint-disable-next-line
      ym('45578973', 'reachGoal', 'Регистрация пользователя')
      // eslint-disable-next-line
      ym('45578973', 'reachGoal', 'form1')
      // eslint-disable-next-line
      dataLayer.push({
        event: 'event-to-ga',
        eventCategory: 'form',
        eventAction: 'form'
      })
      // eslint-disable-next-line
      dataLayer.push({
        event: 'event-to-ga',
        eventCategory: 'Регистрация пользователя',
        eventAction: 'Регистрация пользователя'
      })
    }
  }
}
