export default ({ req, store }) => {
  if (process.server) {
    const ip = req.ip
    store.commit('M_SET_IP', ip)
  }
  if (process.client) {
    localStorage.setItem('ip_address', store.getters.G_IP)
  }
}
