export default function({ store, redirect }) {
  if (!store.getters['auth/G_AUTHENTICATED']) {
    redirect('/admin/login?message=login')
  }
}
