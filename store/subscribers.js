export const actions = {
  async A_NEW_SUBSCRIBE({ commit }, email) {
    try {
      return await this.$axios.post('/api/subscribe/new', { email })
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  },
  async A_GET_SUBSCRIBERS({ commit }) {
    try {
      return await this.$axios.$get('/api/subscribe/list')
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  }
}
