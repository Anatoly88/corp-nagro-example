export const state = () => ({
  S_ARTICLES: []
})

export const mutations = {
  M_ARTICLES(state, articles) {
    state.S_ARTICLES = articles
  }
}

export const actions = {
  async createArticle(
    { commit },
    { title, slug, content, coverUrl, description, date }
  ) {
    try {
      const fd = {
        title,
        content,
        coverUrl,
        description,
        slug,
        date
      }

      return await this.$axios.$post('/api/article/create', fd)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async createNew(
    { commit },
    { title, slug, content, coverUrl, description, date }
  ) {
    try {
      const fd = {
        title,
        content,
        coverUrl,
        description,
        slug,
        date
      }

      return await this.$axios.$post('/api/news/create', fd)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async updateArticle(
    { commit },
    { name, title, slug, content, coverUrl, description, date }
  ) {
    try {
      const fd = {
        title,
        content,
        coverUrl,
        description,
        slug,
        date
      }

      return await this.$axios.$put(`/api/article/update/${name}`, fd)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async updateNew(
    { commit },
    { name, title, slug, content, coverUrl, description, date }
  ) {
    try {
      const fd = {
        title,
        content,
        coverUrl,
        description,
        slug,
        date
      }

      return await this.$axios.$put(`/api/news/update/${name}`, fd)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async fetchArticles({ commit }) {
    try {
      return await this.$axios.$get('/api/article/list')
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async fetchNews({ commit }) {
    try {
      return await this.$axios.$get('/api/news/list')
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async fetchArticle({ commit }, slug) {
    try {
      return await this.$axios.$get(`/api/article/${slug}`)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async fetchNew({ commit }, slug) {
    try {
      return await this.$axios.$get(`/api/news/${slug}`)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async removeArticle({ commit }, id) {
    try {
      return await this.$axios.$delete(`/api/article/remove/${id}`)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async removeNew({ commit }, id) {
    try {
      return await this.$axios.$delete(`/api/news/remove/${id}`)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  }
}

export const getters = {
  G_ARTICLES: (s) => s.S_ARTICLES
}
