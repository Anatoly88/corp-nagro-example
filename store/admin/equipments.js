export const state = () => ({
  S_CONTENT_PAGE: []
})

export const mutations = {
  M_CONTENT_PAGE(state, contentData) {
    state.S_CONTENT_PAGE = contentData
  }
}

export const actions = {
  // Get all equipments-admin list ADMIN
  async A_GET_PAGES_LIST_ADMIN({ commit }) {
    try {
      const pagesData = await this.$axios
        .get('/api/equipments-admin/admin')
        .then(({ data }) => {
          commit('M_CONTENT_PAGE', data)
        })
      return pagesData
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  }, // Get all equipments-admin list
  async A_GET_PAGES_LIST({ commit }) {
    try {
      const pagesData = await this.$axios
        .get('/api/equipments-admin')
        .then(({ data }) => {
          commit('M_CONTENT_PAGE', data)
        })
      return pagesData
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  }, // rm equipment
  async A_REMOVE_EQUIPMENT({ commit }, id) {
    try {
      const pagesData = await this.$axios.$delete(
        `/api/equipments-admin/admin/${id}`
      )
      return pagesData
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  // Get page data by id
  async A_GET_PAGE_BY_ID({ commit }, name) {
    try {
      const pagesData = await this.$axios.get(
        `/api/equipments-admin/admin/${name}`
      )
      return pagesData
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async A_CREATE_PAGE_CONTENT(
    { commit },
    {
      title,
      value,
      price,
      performance,
      content,
      parametrs,
      footer,
      image,
      equipmentType
    }
  ) {
    try {
      const fd = new FormData()

      fd.append('title', title)
      fd.append('value', value)
      fd.append('price', price)
      fd.append('performance', performance)
      fd.append('content', content)
      fd.append('parametrs', parametrs)
      fd.append('footer', footer)
      fd.append('image', image, image.name)
      fd.append('equipmentType', equipmentType)

      return await this.$axios.$post('/api/equipments-admin/admin', fd)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  // Update page data
  async A_UPDATE_PAGE(
    { commit, dispatch },
    { id, title, value, price, performance, content, parametrs, footer }
  ) {
    try {
      const formData = {}

      formData.title = title
      formData.value = value
      formData.price = price
      formData.performance = performance
      formData.content = content
      formData.parametrs = parametrs
      formData.footer = footer

      return await this.$axios.$put(
        `/api/equipments-admin/admin/${id}`,
        formData
      )
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  }
}
export const getters = {
  G_CONTENT_PAGE: (s) => s.S_CONTENT_PAGE
}
