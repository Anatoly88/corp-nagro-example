import instructionsData from '~/static/data/instructions.json'

export const state = () => ({
  S_INSTRUCTION: {}
})

export const mutations = {
  M_INSTRUCTION_DATA(state, instruction) {
    state.S_INSTRUCTION = instruction
  }
}

export const actions = {
  A_GET_INSTRUCTION_DATA_BY_NAME({ commit }, name) {
    const instructionCurrent = instructionsData.find(
      (currentValue) => currentValue.pageName === name
    )
    commit('M_INSTRUCTION_DATA', instructionCurrent)
  }
}

export const getters = {
  G_INSTRUCTION: (s) => s.S_INSTRUCTION
}
