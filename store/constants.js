export const callbackPhone = '8 (800) 707 98 31'
export const callbackPhoneUtm = '8 (800) 707 96 81'
export const SET_CURRENT_TAB = 'UI_SET_CURRENT_TAB'
export const GET_ADMIN_NAV_LIST = [
  // {
  //   name: 'Добавить оборудование',
  //   path: '/admin/create-equipment'
  // },
  // {
  //   name: 'Оборудование',
  //   path: '/admin/equipments-admin'
  // },
  {
    name: 'Добавить новость',
    path: '/admin/create-new',
    icon: 'el-icon-folder-add'
  },
  {
    name: 'Новости',
    path: '/admin/news',
    icon: 'el-icon-news'
  },
  {
    name: 'Добавить статью',
    path: '/admin/create-article',
    icon: 'el-icon-document-add'
  },
  {
    name: 'Статьи',
    path: '/admin/articles',
    icon: 'el-icon-news'
  },
  {
    name: 'Добавить акцию',
    path: '/admin/create-action',
    icon: 'el-icon-position'
  },
  {
    name: 'Акции',
    path: '/admin/actions',
    icon: 'el-icon-data-line'
  },
  {
    name: 'Список подписчиков',
    path: '/admin/subscribers',
    icon: 'el-icon-receiving'
  },
  {
    name: 'Добавить нового пользователя',
    path: '/admin/users',
    icon: 'el-icon-user'
  },
  {
    name: 'Выйти',
    path: '/admin/logout',
    icon: 'el-icon-s-home'
  }
]
