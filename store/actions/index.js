// export const state = () => ({})
//
// export const mutations = () => ({})

export const actions = {
  async create({ commit }, formData) {
    try {
      return await this.$axios.$post('/api/action/create', formData)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async update({ commit }, { id, formData }) {
    try {
      return await this.$axios.$put(`/api/action/update/${id}`, formData)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async fetchAll({ commit }) {
    try {
      const data = (await this.$axios.$get(`/api/action/all`)) || []
      return data
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async fetchById({ commit }, id) {
    try {
      return await this.$axios.$get(`/api/action/${id}`)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async remove({ commit }, id) {
    try {
      return await this.$axios.$delete(`/api/action/remove/${id}`)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  }
}

export const getters = () => ({})
