export const state = () => ({
  S_IP: '',
  S_REVIEWS_SLIDER: [],
  S_REPRESENTATION: [],
  S_PROMO_VIDEO: false,
  S_PRELOADER: true,
  S_EQUIPMENT_LIST: [],
  S_ERROR: null,
  S_MODAL_SHOWED: false,
  S_MODAL_NAME: '',
  S_NAV_LIST: [
    { path: '/', name: 'Главная' },
    { path: '/equipments', name: 'Оборудование' },
    { path: '/video', name: 'Видео' },
    { path: '/news', name: 'Новости' },
    { path: '/articles', name: 'Статьи' },
    { path: '/leasing', name: 'Лизинг' }
  ],
  S_NAV_LIST_OTHERS: [
    { path: '/about', name: 'О компании' },
    { path: '/instructions', name: 'Инструкции' },
    // { path: '/catalog', name: 'Удобрения' },
    { path: '/contacts', name: 'Контакты' }
  ],
  S_NAV_LIST_MOB: [
    { path: '/', name: 'Главная' },
    { path: '/equipments', name: 'Оборудование' },
    // { path: '/catalog', name: 'Удобрения' },
    { path: '/video', name: 'Видео' },
    { path: '/news', name: 'Новости' },
    { path: '/articles', name: 'Статьи' },
    { path: '/about', name: 'О компании' },
    { path: '/instructions', name: 'Инструкции' },
    { path: '/leasing', name: 'Лизинг' },
    { path: '/contacts', name: 'Контакты' }
  ]
})

export const mutations = {
  M_REVIEWS_SLIDER(state, reviews) {
    state.S_REVIEWS_SLIDER = reviews
  },
  M_REPRESENTATION(state, representationList) {
    state.S_REPRESENTATION = representationList
  },
  M_PROMO_VIDEO(state, promoVideoShowed) {
    state.S_PROMO_VIDEO = promoVideoShowed
  },
  M_PRELOADER(state, preloaderDone) {
    state.S_PRELOADER = preloaderDone
  },
  M_EQUIPMENT_DATA(state, equipment) {
    state.S_EQUIPMENT_LIST = equipment
  },
  M_SET_ERROR(state, error) {
    state.S_ERROR = error
  },
  M_REMOVE_ERROR(state) {
    state.S_ERROR = null
  },
  M_MODAL_SHOW(state, showed) {
    state.S_MODAL_SHOWED = showed
  },
  M_MODAL_NAME(state, name) {
    state.S_MODAL_NAME = name
  },
  M_SET_IP(state, ip) {
    state.S_IP = ip
  }
}

export const actions = {
  async imageUploader({ commit }, image) {
    try {
      const fd = new FormData()
      fd.append('file', image, image.name)
      return await this.$axios.post('/api/upload/image', fd)
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  },
  nuxtServerInit({ dispatch }) {
    dispatch('auth/A_LOGIN_AUTO')
  },
  A_GET_REVIEWS_DATA({ commit, dispatch }) {
    const reviews = [
      {
        sources: [
          {
            src: 'https://www.nagro.group/video/review.mp4'
          }
        ],
        poster: require('~/static/images/tmp_slider.jpg'),
        text: 'Видео-отзыв нашего клиента'
      },
      {
        sources: [
          {
            src: 'https://www.nagro.group/video/review_short.mp4'
          }
        ],
        poster: require('~/static/images/tmp_slider.jpg'),
        text: 'Видео-отзыв нашего клиента'
      },
      {
        sources: [
          {
            src: 'https://www.nagro.group/video/review.mp4'
          }
        ],
        poster: require('~/static/images/tmp_slider.jpg'),
        text: 'Видео-отзыв нашего клиента'
      },
      {
        sources: [
          {
            src: 'https://www.nagro.group/video/review.mp4'
          }
        ],
        poster: require('~/static/images/tmp_slider.jpg'),
        text: 'Видео-отзыв нашего клиента'
      }
    ]
    commit('M_REVIEWS_SLIDER', reviews)
  },
  A_GET_REPRESENTATION_LIST({ commit, dispatch }, { updateList }) {
    const representation = [
      {
        address:
          'Дистрибьютор Казахстан, г. Нур-Султан, ул. Куйши Дина, 14, офис 34',
        phone: '+7 7172 34 30 29',
        showed: true,
        logo: 'adotex_logo.svg'
      },
      {
        address:
          'Северо-казахстанская область, г.Петропавловск, ул. Абая, д.29, каб.106',
        phone: '8 (771) 029-11-77',
        showed: true,
        logo: 'contacts_pathfinder.png'
      },
      {
        address:
          'Московская, Ленинградская, Калужская, Рязанская области Республика Крым',
        phone: '8 (800) 707-98-31',
        showed: true
      },
      {
        address: 'Республика Беларусь',
        phone: '8 (800) 707-96-42',
        showed: true
      },
      {
        address: 'Алтайский край',
        phone: '8 (800) 707-96-25',
        showed: true
      },
      {
        address:
          'Архангельская, Вологодская, Ивановская, Костромская, Тверская, Ярославская области',
        phone: '8 (800) 707-96-43',
        showed: false
      },
      {
        address: 'Брянская, Орловская области',
        phone: '8 (800) 707-96-31',
        showed: false
      },
      {
        address: 'Белгородская, Курская область',
        phone: '8 (800) 707-96-32',
        showed: false
      },
      {
        address:
          'Волгоградская, Пензенская, Самарская, Тамбовская, Томская области',
        phone: '8 (800) 707-96-34',
        showed: false
      },
      {
        address: 'Воронежская, Липецкая области',
        phone: '8 (800) 707-95-36',
        showed: false
      },
      {
        address: 'Кемеровская, Новосибирская области',
        phone: '8 (800) 707-96-82',
        showed: false
      },
      {
        address: 'Краснодарский край',
        phone: '8 (800) 707-96-23',
        showed: false
      },
      {
        address: 'Нижегородская, Ульяновская области, Республика Мордовия',
        phone: '8 (800) 707-96-52',
        showed: false
      },
      {
        address: 'Республики Татарстан, Марий Эл, Удмуртия',
        phone: '8 (800) 707-95-63',
        showed: false
      },
      {
        address: 'Республика Башкортостан',
        phone: '8 (800) 707-96-51',
        showed: false
      },
      {
        address: 'Ростовская область',
        phone: '8 (800) 707-96-83',
        showed: false
      },
      {
        address: 'Саратовская область',
        phone: '8 (800) 707-96-41',
        showed: false
      },
      {
        address: 'Ставропольский край',
        phone: '8 (800) 707-96-35',
        showed: false
      },
      {
        address: 'Челябинская, Свердловская, Курганская, Оренбургская области',
        phone: '8 (800) 707-96-74',
        showed: false
      }
    ]
    if (updateList) {
      representation.map((item, index) => {
        if (!item.showed) item.showed = true
      })
      commit('M_REPRESENTATION', representation)
    } else {
      commit('M_REPRESENTATION', representation)
    }
  },
  A_PROMO_VIDEO_SHOWED({ commit }, { showed }) {
    commit('M_PROMO_VIDEO', showed)
  },
  A_PRELOADER_END({ commit }, { load }) {
    commit('M_PRELOADER', load)
  },
  A_GET_EQUIPMENT_DATA({ commit }) {
    const equipmentList = [
      {
        title: 'РУКАС 150'
      }
    ]
    commit('M_EQUIPMENT_DATA', equipmentList)
  },
  A_MODAL_SHOW({ commit }, { modalIsShowed }) {
    commit('M_MODAL_SHOW', modalIsShowed)
  },
  A_MODAL_NAME({ commit }, { name }) {
    commit('M_MODAL_NAME', name)
  }
}

export const getters = {
  G_REVIEWS: (s) => s.S_REVIEWS_SLIDER,
  G_REPRESENTATION: (s) => s.S_REPRESENTATION,
  G_PRELOADER: (s) => s.S_PRELOADER,
  G_EQUIPMENT: (s) => s.S_EQUIPMENT_LIST,
  G_ERROR: (s) => s.S_ERROR,
  G_MODAL_SHOW: (s) => s.S_MODAL_SHOWED,
  G_MODAL_NAME: (s) => s.S_MODAL_NAME,
  G_NAV_LIST: (s) => s.S_NAV_LIST,
  G_NAV_LIST_OTHERS: (s) => s.S_NAV_LIST_OTHERS,
  G_NAV_LIST_MOB: (s) => s.S_NAV_LIST_MOB,
  G_IP: (s) => s.S_IP
}
