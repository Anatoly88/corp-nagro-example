import Cookie from 'cookie'
import Cookies from 'js-cookie'
import jwtDecode from 'jwt-decode'

export const state = () => ({
  S_TOKEN: null,
  S_IS_ADMIN: false
})

export const mutations = {
  M_SET_TOKEN(state, token) {
    state.S_TOKEN = token
  },
  M_SET_ADMIN(state, token) {
    state.S_IS_ADMIN = token
  },
  M_CLEAR_TOKEN(state) {
    state.S_TOKEN = null
  }
}

export const actions = {
  async A_LOGIN({ commit, dispatch }, formdata) {
    try {
      const { token, userId } = await this.$axios.$post(
        '/api/auth/login',
        formdata
      )
      dispatch('A_SET_TOKEN', { token, id: userId })
    } catch (e) {
      console.log('error')
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  },

  async A_LOGIN_ADMIN({ commit, dispatch }, formdata) {
    try {
      const { token, userId } = await this.$axios.$post(
        '/api/auth/admin/login',
        formdata
      )
      dispatch('A_SET_TOKEN', { token, id: userId })
    } catch (e) {
      console.log('error')
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  },

  A_SET_TOKEN({ commit }, { token, id }) {
    this.$axios.setToken(token, 'Bearer')
    commit('M_SET_TOKEN', token)
    Cookies.set('jwt-token', token)
    Cookies.set('userId', id)
  },

  A_LOGOUT({ commit }) {
    this.$axios.setToken(false)
    commit('M_CLEAR_TOKEN')
    Cookies.remove('jwt-token')
    Cookies.remove('userId')
  },

  async A_CREATE_CANDIDATE({ commit, dispatch }, phone) {
    try {
      const { candidateId, candidateCode } = await this.$axios.$post(
        '/api/candidate/create',
        {
          phone
        }
      )
      Cookies.set('candidateId', candidateId)
      return { candidateCode }
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  },

  async A_CHECK_CANDIDATE({ commit, dispatch }, { id, code }) {
    try {
      const { isConfirmed } = await this.$axios.$post('/api/candidate/check', {
        id,
        code
      })

      return isConfirmed
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  },

  async A_CREATE_USER({ commit }, formData) {
    try {
      const fd = new FormData()
      fd.append('login', formData.login)
      fd.append('name', formData.name)
      fd.append('password', formData.password)
      fd.append('lastName', formData.lastName)
      fd.append('company', formData.company)
      fd.append('dateReg', formData.dateReg)
      fd.append('phone', formData.phone)
      fd.append('email', formData.email)
      fd.append('skype', formData.skype)
      fd.append('whatsapp', formData.whatsapp)
      fd.append('telegram', formData.telegram)
      fd.append('country', formData.country)
      fd.append('gender', formData.gender)
      fd.append('birthday', formData.birthday)
      fd.append('imageUrl', formData.imageUrl)
      return await this.$axios.post('/api/auth/create', formData)
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  },

  async A_CREATE_USER_ADMIN({ commit }, formData) {
    try {
      const fd = new FormData()
      fd.append('login', formData.login)
      fd.append('name', formData.name)
      fd.append('password', formData.password)
      fd.append('lastName', formData.lastName)
      fd.append('company', formData.company)
      fd.append('dateReg', formData.dateReg)
      fd.append('phone', formData.phone)
      fd.append('email', formData.email)
      fd.append('skype', formData.skype)
      fd.append('whatsapp', formData.whatsapp)
      fd.append('telegram', formData.telegram)
      fd.append('country', formData.country)
      fd.append('gender', formData.gender)
      fd.append('birthday', formData.birthday)
      fd.append('imageUrl', formData.imageUrl)
      fd.append('role', formData.role)
      return await this.$axios.post('/api/auth/admin/create', formData)
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  },

  async A_UPDATE_USER({ commit }, formData) {
    try {
      const fd = new FormData()
      fd.append('login', formData.login)
      fd.append('name', formData.name)
      fd.append('password', formData.password)
      fd.append('lastName', formData.lastName)
      fd.append('company', formData.company)
      fd.append('dateReg', formData.dateReg)
      fd.append('phone', formData.phone)
      fd.append('email', formData.email)
      fd.append('skype', formData.skype)
      fd.append('whatsapp', formData.whatsapp)
      fd.append('telegram', formData.telegram)
      fd.append('country', formData.country)
      fd.append('gender', formData.gender)
      fd.append('birthday', formData.birthday)
      fd.append('imageUrl', formData.imageUrl)

      return await this.$axios.put('/api/auth/update', formData)
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  },

  A_LOGIN_AUTO({ dispatch }) {
    // получаем строку куки на клиенте и на сервере
    const cookieStr = process.browser
      ? document.cookie
      : this.app.context.req.headers.cookie

    const cookies = Cookie.parse(cookieStr || '') || {}
    const token = cookies['jwt-token']
    const userId = cookies.userId

    if (isJWTValid(token)) {
      dispatch('A_SET_TOKEN', { token, id: userId })
    } else {
      dispatch('A_LOGOUT')
    }
  },

  async A_USER_DATA({ commit }) {
    try {
      const cookieStr = process.browser
        ? document.cookie
        : this.app.context.req.headers.cookie
      const cookies = Cookie.parse(cookieStr || '') || {}
      const userId = cookies.userId
      return await this.$axios.get(`/api/user/${userId}`)
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  }
}

export const getters = {
  G_AUTHENTICATED: (state) => Boolean(state.S_TOKEN),
  G_TOKEN: (state) => state.S_TOKEN
}

function isJWTValid(token) {
  if (!token) {
    return false
  }

  const jwtData = jwtDecode(token) || {}
  const jwtExpires = jwtData.exp || 0
  const timeSession = new Date().getTime() / 1000
  return timeSession < jwtExpires
}
