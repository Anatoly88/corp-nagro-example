export const state = () => ({
  aboutImages: [
    {
      src: '/images/about_slider/about_img_27.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_28.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_29.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_30.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_31.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_32.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_33.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_34.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_35.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },

    {
      src: '/images/about_slider/about_img_21.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_22.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_23.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_24.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_1.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_2.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_4.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_5.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_6.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_7.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_8.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_17.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_18.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_19.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    },
    {
      src: '/images/about_slider/about_img_20.jpg',
      caption: 'Производство Кас, Кас32, Жку, Жсу'
    }
  ]
})

export const getters = {
  G_ABOUT_IMAGES: (state) => state.aboutImages
}
