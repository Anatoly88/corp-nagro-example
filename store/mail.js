export const actions = {
  async A_RECOVERY_PASSWD({ commit }, email) {
    try {
      return await this.$axios.post('/api/mail/recovery', { email })
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  },
  async A_SIGN_UP({ commit }, login) {
    try {
      return await this.$axios.post('/api/mail/sign-up', { login })
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  },
  async A_SEND_SMS({ commit }, message) {
    try {
      return await this.$axios.post('/api/sms', { message })
    } catch (e) {
      commit('M_SET_ERROR', e, { root: true })
      throw e
    }
  }
}
