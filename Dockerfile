FROM node:14-alpine

# RUN mkdir -p /root/app/
RUN mkdir -p /var/www/app/docker/

# WORKDIR /root/app/
WORKDIR /var/www/app/docker/

COPY package*.json /var/www/app/docker/
# RUN npm install --progress=false
RUN npm install && npm cache clean --force --loglevel=error

ENV NODE_ENV=production

# ${NODE_ENV} обращение к переменным

COPY . /var/www/app/docker/

RUN npm run build

ENV HOST 0.0.0.0
EXPOSE 3000

CMD ["npm", "start"]
